package com.foreveross;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * springcloud的消费方
 * 使用Feign作为消费方和生产方的中间调度对象,Feign实际是整合了Ribbon的
 * 1.引导类上声明@EnableFeignClients  开启Feign功能
 * 2.建立接口,并声明@FeignClient("调用的服务器组名") @RequestMapping 声明要调度的方法  @RequestParam("name") 需求的参数
 * @author Administrator
 * 
 */
@EnableDiscoveryClient //指定该消费者要去注册中心寻找服务
@SpringBootApplication
@EnableFeignClients //开启Feign功能
public class FeignApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeignApplication.class, args);
	}	
}
