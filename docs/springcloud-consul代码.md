#SpringCloud基于Consul注册中心
>Consul是基于C/S架构的注册中心，所以作为注册中心需要下载consul,推荐使用版本不超过0.9.3的，由于1.0以上版本存在健康检查Rest地址需要重新配置的问题
##步骤
1. 下载安装[consul](https://www.consul.io/downloads.html), 在window系统下, cmd执行开启,命令 consul agent -dev
2. 创建项目consul-server，即服务提供方（provider），加入maven依赖

		<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
		  <modelVersion>4.0.0</modelVersion>
		  <groupId>com.foreveross</groupId>
		  <artifactId>consul-server</artifactId>
		  <version>0.0.1-SNAPSHOT</version>
  
		  	<parent>
		        <groupId>org.springframework.boot</groupId>
		        <artifactId>spring-boot-starter-parent</artifactId>
		        <version>1.5.2.RELEASE</version>
		        <relativePath/> <!-- lookup parent from repository -->
		    </parent>
		
		    <properties>
		        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		        <java.version>1.8</java.version>
		    </properties>
		
		    <dependencies>
		    	<!-- 提供各类接口，用于服务的健康检测 -->
		    	<dependency> 
				    <groupId>org.springframework.boot</groupId> 
				    <artifactId>spring-boot-starter-actuator</artifactId> 
				</dependency>
				<!-- springcloud集成consul包 -->
		        <dependency>
		            <groupId>org.springframework.cloud</groupId>
		            <artifactId>spring-cloud-starter-consul-discovery</artifactId>
		        </dependency>
		        <!-- 启动web容器依赖包 -->
		        <dependency>
		            <groupId>org.springframework.boot</groupId>
		            <artifactId>spring-boot-starter-web</artifactId>
		        </dependency>
				<!-- 测试包 -->
		        <dependency>
		            <groupId>org.springframework.boot</groupId>
		            <artifactId>spring-boot-starter-test</artifactId>
		            <scope>test</scope>
		        </dependency>
		    </dependencies>
		
		    <dependencyManagement>
		        <dependencies>
		            <dependency>
		                <groupId>org.springframework.cloud</groupId>
		                <artifactId>spring-cloud-dependencies</artifactId>
		                <version>Dalston.RELEASE</version>
		                <type>pom</type>
		                <scope>import</scope>
		            </dependency>
		        </dependencies>
		    </dependencyManagement>
		
		    <build>
		        <plugins>
		            <plugin>
		                <groupId>org.springframework.boot</groupId>
		                <artifactId>spring-boot-maven-plugin</artifactId>
		            </plugin>
		        </plugins>
		    </build>
		</project>
3. application.properties的配置信息

		#consul是一个C/S架构软件，默认端口是8500
		spring.cloud.consul.host=localhost
		spring.cloud.consul.port=8500
		#健康检测路径， 注册的时候会调用此路径，若可以则通过了健康检测
		spring.cloud.consul.discovery.healthCheckPath=/health
		#健康检测的间隔，默认10s，会发送一个http请求，如果没响应，标记服务critical
		spring.cloud.consul.discovery.healthCheckInterval=15s
		#注册时候的实例化名字 sevice的名字,只是一个服务的标记，组员名称
		#spring.cloud.consul.discovery.instance-id=consul-miya1
		#组的名称，可用负载均衡，同组下多个生产者
		spring.application.name=consul-miya
		server.port=8504

4. 服务提供方各层代码

	引导类：
		
			package com.foreveross;
		
			import org.springframework.boot.SpringApplication;
			import org.springframework.boot.autoconfigure.SpringBootApplication;
			import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
			/**
			 * consul作为注册中心
			 * consul是个C/S架构的注册中心，不同于eureka，无需我们自己开启注册中心，直接启动C/S架构的注册中心即可
			 * 另外最好使用版本最高不要操作0.9.3的consul注册中心
			 * 默认的端口是8500
			 * @author Administrator
			 *
			 */
			
			@SpringBootApplication
			@EnableDiscoveryClient
			public class ConsulServerApplication {
			
			public static void main(String[] args) {
				SpringApplication.run(ConsulServer.class, args);
			}
		}


	控制层：

			package com.foreveross.controller;
		
			import org.springframework.beans.factory.annotation.Value;
			import org.springframework.web.bind.annotation.RequestMapping;
			import org.springframework.web.bind.annotation.RequestParam;
			import org.springframework.web.bind.annotation.RestController;
			
			@RestController
			public class ConsulController {
			
			@RequestMapping("/hi")
			public String hi(@RequestParam("name") String name) {
				return "a response from consul's provider:"+name;
			}
			
			@Value("${server.port}")
			private String port;
			
			@RequestMapping("/hello")
			public String hello() {
				return "the server port is:"+port;
			}
		}

5. 创建项目consul-client，即服务消费方（consummer）,加入maven依赖

		<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
		  <modelVersion>4.0.0</modelVersion>
		  <groupId>com.foreveross</groupId>
		  <artifactId>consul-client</artifactId>
		  <version>0.0.1-SNAPSHOT</version>
		  
		  <parent>
		        <groupId>org.springframework.boot</groupId>
		        <artifactId>spring-boot-starter-parent</artifactId>
		        <version>1.5.2.RELEASE</version>
		        <relativePath/> <!-- lookup parent from repository -->
		    </parent>
		
		    <properties>
		        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		        <java.version>1.8</java.version>
		    </properties>
				
		    <dependencies>
		    	<!-- 服务检测的包，心跳之类的 -->
		    	<dependency> 
				    <groupId>org.springframework.boot</groupId> 
				    <artifactId>spring-boot-starter-actuator</artifactId> 
				</dependency>
		    	<!-- 加入在consul上发现服务的依赖 -->
		    	 <dependency>
		            <groupId>org.springframework.cloud</groupId>
		            <artifactId>spring-cloud-starter-consul-discovery</artifactId>
		        </dependency>
		        
		        <dependency>
		            <groupId>org.springframework.cloud</groupId>
		            <artifactId>spring-cloud-starter-feign</artifactId>
		        </dependency>
		        <dependency>
		            <groupId>org.springframework.boot</groupId>
		            <artifactId>spring-boot-starter-web</artifactId>
		        </dependency>
		
		        <dependency>
		            <groupId>org.springframework.boot</groupId>
		            <artifactId>spring-boot-starter-test</artifactId>
		            <scope>test</scope>
		        </dependency>
		    </dependencies>
		
		    <dependencyManagement>
		        <dependencies>
		            <dependency>
		                <groupId>org.springframework.cloud</groupId>
		                <artifactId>spring-cloud-dependencies</artifactId>
		              <!--   <version>Dalston.RC1</version> -->
		              <version>Edgware.RELEASE</version>
		                <type>pom</type>
		                <scope>import</scope>
		            </dependency>
		        </dependencies>
		    </dependencyManagement>
		
		    <build>
		        <plugins>
		            <plugin>
		                <groupId>org.springframework.boot</groupId>
		                <artifactId>spring-boot-maven-plugin</artifactId>
		            </plugin>
		        </plugins>
		    </build>
		
		    <repositories>
		        <repository>
		            <id>spring-milestones</id>
		            <name>Spring Milestones</name>
		            <url>https://repo.spring.io/milestone</url>
		            <snapshots>
		                <enabled>false</enabled>
		            </snapshots>
		        </repository>
		    </repositories>
		</project>

6. 配置文件application.properties

		#指定服务器的名称
		spring.application.name=consul-client
		#服务器端口
		server.port=4001
		#连接注册中心的地址，这里使用consul
		spring.cloud.consul.host=localhost
		spring.cloud.consul.port=8500
		spring.cloud.consul.discovery.healthCheckPath=/health
7. 各层代码

	引导类

		package com.foreveross;
		
		import org.springframework.boot.SpringApplication;
		import org.springframework.boot.autoconfigure.SpringBootApplication;
		import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
		import org.springframework.cloud.netflix.feign.EnableFeignClients;
		
		/**
		 * springcloud的消费方
		 * 使用Feign作为消费方和生产方的中间调度对象,Feign实际是整合了Ribbon的
		 * 1.引导类上声明@EnableFeignClients  开启Feign功能
		 * 2.建立接口,并声明@FeignClient("调用的服务器组名") @RequestMapping 声明要调度的方法  @RequestParam("name") 需求的参数
		 * @author Administrator
		 * 
		 */
		@EnableDiscoveryClient //指定该消费者要去注册中心寻找服务
		@SpringBootApplication
		@EnableFeignClients //开启Feign功能
		public class FeignApplication {
		
			public static void main(String[] args) {
				SpringApplication.run(FeignApplication.class, args);
			}	
		}

	控制层

		package com.foreveross.controller;
		
		import java.util.logging.Level;
		import java.util.logging.Logger;
		
		import org.springframework.beans.factory.annotation.Autowired;
		import org.springframework.web.bind.annotation.RequestMapping;
		import org.springframework.web.bind.annotation.RequestParam;
		import org.springframework.web.bind.annotation.RestController;
		
		import com.foreveross.service.FeignService;
		
		
		
		/**
		 * 控制层调用service层
		 * @author Administrator
		 *
		 */
		@RestController
		public class FeignController {
			
			private static final Logger LOG = Logger.getLogger(FeignController.class.getName());
			
			@Autowired
			private FeignService feignService;
			
			@RequestMapping("/hello")
			public String hello(@RequestParam String name) {
				//LOG.log(Level.INFO,"feign has accepted a request......");
				return feignService.hello(name);
			}
			
			
			@RequestMapping("/test")
			public String test() {
				LOG.log(Level.INFO,"feign has accepted a request......");
				return feignService.test();
			}
		}
	
	调用服务接口：

		package com.foreveross.service;
		
		import org.springframework.cloud.netflix.feign.FeignClient;
		import org.springframework.web.bind.annotation.RequestMapping;
		import org.springframework.web.bind.annotation.RequestMethod;
		import org.springframework.web.bind.annotation.RequestParam;
		
		/**
		 * 使用Feign作为消费方与生产方的中间对象,可以不用连接url,直接声明要连接的服务器的名称即可
		 * 由此对象可以完成负载均衡
		 * @author Administrator
		 *
		 */
		
		//@FeignClient("eureka-client")//声明接口
		@FeignClient(name="consul-miya")//
		public interface FeignService {
			
			//@RequestMapping(value = "/hi",method=RequestMethod.GET) //连接url
			@RequestMapping(value="/hi",method=RequestMethod.GET)
			public String hello(@RequestParam("name") String name); //需要的参数 实际为Http协议的key=value形式
		
			@RequestMapping(value="/hello")
			public String test();
			
			/*@RequestMapping(value="/client",method=RequestMethod.GET)
			public String client();*/
		}

访问：http://localhost:4001/hello?name=abc
得到响应：a response from consul's provider:abc